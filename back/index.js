const express = require("express");
const bodyParser = require("body-parser");
const Message = require("./models/message");
const connectDB = require("./config/db");
const app = express();
const port = 8080;
app.use(bodyParser.json());
connectDB();
app.get("/data", async (req, res) => {
  let messages = await Message.find();
  res.send(messages);
});
app.post("/data", async function (req, res) {
  console.log(req.body);
  const message = new Message({ message: req.body.message });
  console.log;
  message.save().then((res) => console.log(res));
  res.send("POST request to the homepage");
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
