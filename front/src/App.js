import { useEffect, useState } from "react";
import axios from "axios";
import {
  Button,
  Container,
  Row,
  Col,
  ListGroup,
  InputGroup,
  FormControl,
} from "react-bootstrap";

function App() {
  const [newMessage, setNewMessage] = useState("");
  const [messages, setMessages] = useState([]);

  function onSaveMessage() {
    axios.post("/data", { message: newMessage }).then((res) => {
      if (res.status == 200) {
        setMessages([newMessage, ...messages]);
        setNewMessage("");
      } else {
        console.log("error");
        console.log(res);
      }
    });
  }

  async function getData() {
    let response = await axios.get("/data");
    let messages = response.data.reverse();
    setMessages(messages.map((msg) => msg.message));
  }
  useEffect(() => {
    getData();
  }, []);
  function makeid() {
    var text = "";
    var possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    text += Date.now();
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
  return (
    <div className="App">
      <Container fluid>
        <Container>
          <input
            onChange={(e) => setNewMessage(e.target.value)}
            placeholder="Message"
            name="message"
            value={newMessage}
          />
          <button onClick={onSaveMessage}>Save Message</button>
          {messages.length > 0 ? (
            messages.map((message) => {
              return (
                <div key={makeid()}>
                  <p>{message}</p>
                </div>
              );
            })
          ) : (
            <></>
          )}
        </Container>
      </Container>
    </div>
  );
}

export default App;
